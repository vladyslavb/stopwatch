//
//  ViewController.m
//  StopwatchTest
//
//  Created by Vladyslav Bedro on 7/25/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

//outlets
@property (weak, nonatomic) IBOutlet UILabel* timerLabel;
@property (weak, nonatomic) IBOutlet UIButton* resetButton;
@property (weak, nonatomic) IBOutlet UIButton* startButton;

//properties
@property (strong, nonatomic) NSTimer* timer;
@property (assign, nonatomic) BOOL isRunning;
@property (assign, nonatomic) NSUInteger counter;

@end

@implementation ViewController


#pragma mark - Life cycle -

- (void) viewDidLoad
{
    [super viewDidLoad];
    [self configurateSettingsForTimer];
}


#pragma mark - Memory managment -

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Actions -

- (IBAction) onStartButtonPressed: (UIButton*) sender
{
    if (self.isRunning == NO)
    {
        self.isRunning = YES;
        [self.startButton setTitle: @"STOP"
                                   forState: UIControlStateNormal];
        
        if (self.timer == nil)
        {
            self.timer = [NSTimer scheduledTimerWithTimeInterval: 0.01
                                                                                              target: self
                                                                                           selector: @selector(updateTimer)
                                                                                           userInfo: nil
                                                                                            repeats: YES];
        }
    }
    else
    {
        [self stopTimer];
    }
}

- (IBAction) onResetButtonPressed: (UIButton*) sender
{
    [self stopTimer];
    
    self.counter = 0;
    self.timerLabel.text = @"00:00.00";
}


#pragma mark - Internal methods -

- (void) updateTimer
{
    self.counter++;
    
    NSUInteger minutes      = floor(self.counter / 100 / 60);
    NSUInteger seconds     = floor(self.counter / 100);
    NSUInteger mSeconds  =         self.counter % 100;
    
    if (seconds >= 60)
    {
        seconds %= 60;
    }
    
    self.timerLabel.text = [NSString stringWithFormat: @"%02lu:%02lu.%02lu",
                                                                                         (unsigned long)minutes,
                                                                                         (unsigned long)seconds,
                                                                                         (unsigned long)mSeconds];
}

- (void) stopTimer
{
    self.isRunning = NO;
    self.timer         = nil;
    [self.timer invalidate];
    [self.startButton setTitle: @"START"
                               forState: UIControlStateNormal];
}

- (void) configurateSettingsForTimer
{
    self.isRunning = NO;
    self.counter     = 0;
    self.timerLabel.text = @"00:00.00";
    
    self.startButton.layer.cornerRadius = 50;
    self.resetButton.layer.cornerRadius = 50;
}




@end















